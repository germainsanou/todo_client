import React from 'react';
import EventsIndex from './components/events/EventsIndex';
import EventsShow from './components/events/EventsShow';
import TodoList from './components/todos/TodoList';
import 'bootstrap/dist/css/bootstrap.min.css';
// import jquery from 'jquery';
// import { Navbar, Nav, NavDropdown, Form,  FormControl, Button, Table} from 'react-bootstrap';
import Welcom from './Welcom';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div>
      <Router>
        <div className="navbar navbar-expand-sm navbar-dark bg-dark">
          <Link to="/" className="navbar-brand orange">Home</Link>
          <button className="navbar-toggler" data-toggle="collapse" data-target="#navbar-content">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbar-content">
            <ul className="navbar-nav mx-auto">
              <li className="nav-item">
            	 <Link to="/" className="nav-link">Home</Link>
              </li>
              <li className="navbar-item">
                <Link to="/events" className="nav-link">Events</Link>  
              </li>
              <li className="navbar-item">
                <Link to="/todos" className="nav-link">Todos</Link>  
              </li>
            </ul>
          </div>
        </div>

        <div className="container mt-3">
         	<Route path="/" exact component={Welcom}/>
          <Route path="/events" exact component={EventsIndex}/>
  	      <Route path="/todos" exact component={TodoList}/>
  	      <Route path="/events/:id" exact component={props => <EventsShow{...props}/>}/>
        </div>
      </Router>
    </div>
  );
}

export default App;
