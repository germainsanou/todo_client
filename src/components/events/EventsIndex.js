import React from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'

class EventsIndex extends React.Component{
	constructor () {
		super()
		this.state = {
			events: []
		}
	}

	UNSAFE_componentWillMount(){
		axios.get('/api/events').then(response => {
			this.setState({
				events: response.data
			})
		})
	}

	render () {
		return (
			<div>
				{this.state.events.map((event, index) => {
					return(
						<li key={index}><Link to={'/events/' + event.id}>{event.name}</Link></li>
					)
				})}
			</div>
		)
	}
}

export default EventsIndex