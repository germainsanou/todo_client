import React,{Component} from 'react'
import axios from 'axios'

class EventsShow extends Component{

	constructor (props) {
		super(props)
		this.state = {
			event: {}
		}
	}

	componentDidMount(){
		axios.get("/api/events/"+this.props.match.params.id).then(response => {
			this.setState({
				event: response.data
			})
		}).catch(errors => {
			console.log(errors)
		})
	}

	render () {
		return (
			<div>
				<h1 className="lead">{this.state.event.name}</h1>
				<p>{this.state.event.body}</p>
			</div>
		)
	}
}

export default EventsShow