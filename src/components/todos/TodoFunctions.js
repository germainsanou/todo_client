import axios from 'axios'

export const getItems = () => {
  return axios
  .get('/api/todos')
  .then(response => {
      return response.data
  })

}

export const addItem = (name) => {
  return axios
  .post('/api/todos', {
    name: name
  })
  .then(response => {
    return response.data
  })
  .catch(errors => {
    console.log(errors)
  })
}

export const deleteItem = (id) => {
  return axios
  .delete('/api/todos/' + id, {headers: {'Content-Type': 'application/json'}})
  .then(response => {
    return response.data
  })
  .catch(errors => {
    console.log(errors)
  })
}

export const updateItem = (id, name) => {
  return axios
  .put('/api/todos/' + id, {name: name})
  .then(response => {
    return response.data
  })
  .catch(errors => {
    console.log(errors)
  })
}
