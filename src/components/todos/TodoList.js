import React,{Component} from 'react'
import {getItems, addItem, deleteItem, updateItem} from './TodoFunctions'

class TodoList extends Component{
	constructor(props){
		super(props)
		this.state = {
			name: '',
			updateId: '',
			buttonLabel: '',
			items: [],
			permitUpdate: false
		}
	}

	UNSAFE_componentWillMount(){
		this.fetchAll()
	}

	render () {
		return (
			<div className="row">
				<div className="col-md-6 mx-auto">
					<div className="my-2 d-flex justify-content-between">
						<input type="text"
							className="col-9"
							value={this.state.name}
							name="name"
							onChange={ this.onChange.bind(this)}/>
						<button onClick={(e) => this.addOrUpdateTodo(e)} className="d-inline col-2 btn btn-success btn-sm">
							{this.state.buttonLabel}</button>
					</div>
					<table className="table table-dark table-sm table-striped">
					<thead></thead>
					<tbody>
					{this.state.items.map((item,index) => {
						return(
							<tr key={item.id} id={item.id}>
							<td>{item.name}</td>
							<td>{index+1}</td>
							<td>
							<button className="btn btn-sm btn-primary mr-1"
							id={index} onClick={this.InitUpdate.bind(this)}>update</button>
							<button className="btn btn-sm btn-danger" id={item.id} onClick={this.deleteTodo.bind(this)}>x</button>
							</td>
							</tr>
						)
					})}
					</tbody>
					</table>
				</div>
			</div>
		)
	}

	// PERSONAL FUNCTIONS
	onChange(event){
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	addOrUpdateTodo(event){
		event.preventDefault()
		if (this.state.name !== '') {
			if (this.state.permitUpdate) {
				updateItem(this.state.updateId, this.state.name).then(response => {
					this.fetchAll()
				}).catch(errors => {
					console.log(errors)
				})
			}else {
				addItem(this.state.name).then(response => {
					this.fetchAll()
				})
			}
		}else {
			this.setState({
				buttonLabel: 'Add',
				permitUpdate: false
			})
		}
	}

	fetchAll() {
		getItems().then(data => {
			this.setState({
				items: data,
				name: '',
				buttonLabel: 'Add',
				permitUpdate: false,
				updateId: ''
			})
		})
	}

	deleteTodo(event){
		event.preventDefault()
		deleteItem(event.target.id).then(response => {
			this.fetchAll()
		}).catch(errors => {
			console.log(errors)
		})

	}

	InitUpdate(event){
		event.preventDefault()
		const items = this.state.items
		const item = items[event.target.id]
		this.setState({
			name: item.name,
			buttonLabel: 'update',
			permitUpdate: true,
			updateId: event.target.parentElement.parentElement.id
		})
	}

}

export default TodoList
